package com.rappi.carlos_reyes_android.constants;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * <p>
 * -
 * database constants
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	3/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public class ConstansDatabase {

    /** Constant that defines the name of the database **/
    public static final String NOMBRE_BD = "prueba-android.db";

    /**Constant that defines the version of the database **/
    public static final int DATABASE_VERSION = 5;

}
