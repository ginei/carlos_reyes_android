package com.rappi.carlos_reyes_android.constants;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 *
 * - Contains the communication constants
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class ConstantCommunication {

    /**
     * url webservice notice
     */
    public static final String URL_API_REDDIT = "https://www.reddit.com/";

    /**
     * timeout connection
     */
    public static final int TIME_OUT= 30;

    /**
     * timeout connection read
     */
    public static final int TIME_OUT_READ= 30;


}
