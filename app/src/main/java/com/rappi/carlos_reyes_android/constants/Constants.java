package com.rappi.carlos_reyes_android.constants;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * <p>
 * -
 * Constants for sharing information between views
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	2/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public class Constants {

    public static final String DATA_NOTICE = "dataNotice";
}
