package com.rappi.carlos_reyes_android.infrastructure.service;

import com.rappi.carlos_reyes_android.infrastructure.model.Reddit;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Interfaz para consumir los servicios de la aplicación
 *
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public interface ClientService {

    @GET("reddits.json")
    Observable<Response<Reddit>> getNotices();

}
