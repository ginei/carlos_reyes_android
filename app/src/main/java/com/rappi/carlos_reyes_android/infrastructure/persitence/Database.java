package com.rappi.carlos_reyes_android.infrastructure.persitence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.rappi.carlos_reyes_android.constants.ConstansDatabase;
import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;

import java.util.ArrayList;
import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * <p>
 * -
 * Manage application database logic
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	3/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public class Database extends SQLiteOpenHelper implements IDatabase {

    private String TAG = Database.class.getSimpleName();

    private static Database sInstance;

    private String CREATE_TABLE_NOTICE = "CREATE TABLE IF NOT EXISTS " + IDatabaseTables.TABLE_NOTICES + " (" +
            IDatabaseTables.KEY_ID + "	TEXT PRIMARY KEY, " +
            IDatabaseTables.KEY_TITLE + "	TEXT , " +
            IDatabaseTables.KEY_ICON_IMG + "	TEXT , " +
            IDatabaseTables.KEY_ICON_SIZE0 + "	NUMERIC , " +
            IDatabaseTables.KEY_ICON_SIZE1 + "	NUMERIC , " +
            IDatabaseTables.KEY_HEADER_IMG + "	TEXT , " +
            IDatabaseTables.KEY_HEADER_SIZE0 + "	NUMERIC , " +
            IDatabaseTables.KEY_HEADER_SIZE1 + "	NUMERIC , " +
            IDatabaseTables.KEY_CATEGORY + "	TEXT , " +
            IDatabaseTables.KEY_DATE_TIME + "	TEXT , " +
            IDatabaseTables.KEY_USER + "	TEXT , " +
            IDatabaseTables.KEY_DESCRIPTION_SHORT + "	TEXT , " +
            IDatabaseTables.KEY_DESCRIPTION + "	TEXT , " +
            IDatabaseTables.KEY_SUSCRIBERS + "	TEXT  " +
            ");";


    public static synchronized Database getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new Database(context.getApplicationContext());
        }
        return sInstance;
    }


    private Database(Context context) {
        super(context, ConstansDatabase.NOMBRE_BD, null, ConstansDatabase.DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NOTICE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + IDatabaseTables.TABLE_NOTICES);
        onCreate(db);
    }


    /**
     * create new news in the database
     * @param notice object notice
     * @throws Exception
     */
    @Override
    public void insertNotices(Notice notice) throws Exception {

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        Log.d(TAG, " -------insert new noticia-------------");
        Log.d(TAG, " " + notice.getId());
        Log.d(TAG, " -------------------------");
        try {
            /*
            Armamos el objeto con los valores a insertar en la tabla
             */
            ContentValues values = new ContentValues();
            values.put(IDatabaseTables.KEY_ID, notice.getId());
            values.put(IDatabaseTables.KEY_TITLE, notice.getTitle());
            values.put(IDatabaseTables.KEY_ICON_IMG, notice.getIcon_img());
            if (notice.getIcon_size() != null &&
                    notice.getIcon_size().length > 1) {
                values.put(IDatabaseTables.KEY_ICON_SIZE0, notice.getIcon_size()[0]);
                values.put(IDatabaseTables.KEY_ICON_SIZE1, notice.getIcon_size()[1]);
            }

            values.put(IDatabaseTables.KEY_HEADER_IMG, notice.getHeader_img());

            if (notice.getHeader_size() != null &&
                    notice.getHeader_size().length > 1) {
                values.put(IDatabaseTables.KEY_HEADER_SIZE0, notice.getHeader_size()[0]);
                values.put(IDatabaseTables.KEY_HEADER_SIZE1, notice.getHeader_size()[1]);
            }

            values.put(IDatabaseTables.KEY_CATEGORY, notice.getAdvertiser_category());
            values.put(IDatabaseTables.KEY_DATE_TIME, notice.getCreated());
            values.put(IDatabaseTables.KEY_USER, notice.getDisplay_name());
            values.put(IDatabaseTables.KEY_DESCRIPTION_SHORT, notice.getPublic_description().equals("") ?
                    notice.getSubmit_text() : notice.getPublic_description());
            values.put(IDatabaseTables.KEY_DESCRIPTION, notice.getDescription());
            values.put(IDatabaseTables.KEY_SUSCRIBERS, notice.getSubscribers());

            int id = (int) db.insertWithOnConflict(IDatabaseTables.TABLE_NOTICES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                Log.d(TAG, "update notice");
                db.update(IDatabaseTables.TABLE_NOTICES, values, IDatabaseTables.KEY_ID + "='" + notice.getId() + "'", null);
            }

            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.e(TAG, "Error at insert notice" + e);
        } finally {
            db.endTransaction();
            Log.d(TAG, " end insert notice");
        }

    }


    /**
     * Get news list
     * @return List notices
     * @throws Exception
     */
    @Override
    public List<Children> getNotices() throws Exception {

        List<Children> notices = new ArrayList<>();
        Children children;

        String query = String.format("SELECT * FROM %s",
                IDatabaseTables.TABLE_NOTICES);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Notice notice;
        try {
            if (cursor.moveToFirst()) {
                do {
                    children = new Children();
                    notice = new Notice();
                    children.setNotice(notice);

                    children.getNotice().setId(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_ID)));
                    children.getNotice().setTitle(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_TITLE)));
                    children.getNotice().setIcon_img(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_ICON_IMG)));

                    Integer iconSize0 = cursor.getInt(cursor.getColumnIndex(IDatabaseTables.KEY_ICON_SIZE0));
                    Integer iconSize1 = cursor.getInt(cursor.getColumnIndex(IDatabaseTables.KEY_ICON_SIZE1));
                    Integer headerSize0 = cursor.getInt(cursor.getColumnIndex(IDatabaseTables.KEY_HEADER_SIZE0));
                    Integer headerSize1 = cursor.getInt(cursor.getColumnIndex(IDatabaseTables.KEY_HEADER_SIZE1));

                    int[] size = {iconSize0, iconSize1};
                    children.getNotice().setIcon_size(size);
                    int[] sizeH = {headerSize0, headerSize1};
                    children.getNotice().setHeader_size(sizeH);
                    children.getNotice().setHeader_img(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_HEADER_IMG)));

                    children.getNotice().setAdvertiser_category(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_CATEGORY)));

                    children.getNotice().setCreated(cursor.getDouble(cursor.getColumnIndex(IDatabaseTables.KEY_DATE_TIME)));
                    children.getNotice().setDisplay_name(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_USER)));
                    children.getNotice().setPublic_description(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_DESCRIPTION_SHORT)));
                    children.getNotice().setDescription(cursor.getString(cursor.getColumnIndex(IDatabaseTables.KEY_DESCRIPTION)));
                    children.getNotice().setSubscribers(cursor.getLong(cursor.getColumnIndex(IDatabaseTables.KEY_SUSCRIBERS)));

                    Log.d(TAG, "---------------------------");
                    Log.d(TAG, "" + children.getNotice().toString());
                    Log.d(TAG, "---------------------------");

                    notices.add(children);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Error getting notices");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return notices;
    }
}
