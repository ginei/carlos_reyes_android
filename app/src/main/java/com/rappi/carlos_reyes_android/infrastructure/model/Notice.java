package com.rappi.carlos_reyes_android.infrastructure.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * <p/>
 * - Modelo que contiene la informacion de la noticia
 * <p>
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class Notice implements Serializable {

    private String banner_img;
    private boolean user_sr_theme_enabled;
    private String submit_text_html;
    private boolean user_is_banned;
    private boolean wiki_enabled;
    private boolean show_media;
    private String id;
    private String submit_text;
    private String display_name;
    private String header_img;
    private String description_html;
    private String title;
    private boolean collapse_deleted_comments;
    private boolean over18;
    private String public_description_html;
    private boolean spoilers_enabled;
    private int[] icon_size;
    private String suggested_comment_sort;
    private String icon_img;
    private String header_title;
    private String description;
    private boolean user_is_muted;
    private String submit_link_label;
    private int[] header_size;
    private boolean public_traffic;
    private String accounts_active;
    private Long subscribers;
    private String submit_text_label;
    private String lang;
    private boolean user_is_moderator;
    private String key_color;
    private String name;
    private Double created;
    private String url;
    private boolean quarantine;
    private boolean hide_ads;
    private Double created_utc;
    private int[] banner_size;
    private boolean user_is_contributor;
    private String advertiser_category;
    private String public_description;
    private boolean show_media_preview;
    private int comment_score_hide_mins;
    private String subreddit_type;
    private String submission_type;
    private boolean user_is_subscriber;

    public Notice() {
    }


    @Override
    public String toString() {
        return "Notice{" +
                "banner_img='" + banner_img + '\'' +
                ", user_sr_theme_enabled=" + user_sr_theme_enabled +
                ", submit_text_html='" + submit_text_html + '\'' +
                ", user_is_banned=" + user_is_banned +
                ", wiki_enabled=" + wiki_enabled +
                ", show_media=" + show_media +
                ", id='" + id + '\'' +
                ", submit_text='" + submit_text + '\'' +
                ", display_name='" + display_name + '\'' +
                ", header_img='" + header_img + '\'' +
                ", description_html='" + description_html + '\'' +
                ", title='" + title + '\'' +
                ", collapse_deleted_comments=" + collapse_deleted_comments +
                ", over18=" + over18 +
                ", public_description_html='" + public_description_html + '\'' +
                ", spoilers_enabled=" + spoilers_enabled +
                ", icon_size=" + Arrays.toString(icon_size) +
                ", suggested_comment_sort='" + suggested_comment_sort + '\'' +
                ", icon_img='" + icon_img + '\'' +
                ", header_title='" + header_title + '\'' +
                ", description='" + description + '\'' +
                ", user_is_muted=" + user_is_muted +
                ", submit_link_label='" + submit_link_label + '\'' +
                ", header_size=" + Arrays.toString(header_size) +
                ", public_traffic=" + public_traffic +
                ", accounts_active='" + accounts_active + '\'' +
                ", subscribers=" + subscribers +
                ", submit_text_label='" + submit_text_label + '\'' +
                ", lang='" + lang + '\'' +
                ", user_is_moderator=" + user_is_moderator +
                ", key_color='" + key_color + '\'' +
                ", name='" + name + '\'' +
                ", created=" + created +
                ", url='" + url + '\'' +
                ", quarantine=" + quarantine +
                ", hide_ads=" + hide_ads +
                ", created_utc=" + created_utc +
                ", banner_size=" + Arrays.toString(banner_size) +
                ", user_is_contributor=" + user_is_contributor +
                ", advertiser_category='" + advertiser_category + '\'' +
                ", public_description='" + public_description + '\'' +
                ", show_media_preview=" + show_media_preview +
                ", comment_score_hide_mins=" + comment_score_hide_mins +
                ", subreddit_type='" + subreddit_type + '\'' +
                ", submission_type='" + submission_type + '\'' +
                ", user_is_subscriber=" + user_is_subscriber +
                '}';
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public boolean isUser_sr_theme_enabled() {
        return user_sr_theme_enabled;
    }

    public void setUser_sr_theme_enabled(boolean user_sr_theme_enabled) {
        this.user_sr_theme_enabled = user_sr_theme_enabled;
    }

    public String getSubmit_text_html() {
        return submit_text_html;
    }

    public void setSubmit_text_html(String submit_text_html) {
        this.submit_text_html = submit_text_html;
    }

    public boolean isUser_is_banned() {
        return user_is_banned;
    }

    public void setUser_is_banned(boolean user_is_banned) {
        this.user_is_banned = user_is_banned;
    }

    public boolean isWiki_enabled() {
        return wiki_enabled;
    }

    public void setWiki_enabled(boolean wiki_enabled) {
        this.wiki_enabled = wiki_enabled;
    }

    public boolean isShow_media() {
        return show_media;
    }

    public void setShow_media(boolean show_media) {
        this.show_media = show_media;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubmit_text() {
        return submit_text;
    }

    public void setSubmit_text(String submit_text) {
        this.submit_text = submit_text;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getHeader_img() {
        return header_img;
    }

    public void setHeader_img(String header_img) {
        this.header_img = header_img;
    }

    public String getDescription_html() {
        return description_html;
    }

    public void setDescription_html(String description_html) {
        this.description_html = description_html;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCollapse_deleted_comments() {
        return collapse_deleted_comments;
    }

    public void setCollapse_deleted_comments(boolean collapse_deleted_comments) {
        this.collapse_deleted_comments = collapse_deleted_comments;
    }

    public boolean isOver18() {
        return over18;
    }

    public void setOver18(boolean over18) {
        this.over18 = over18;
    }

    public String getPublic_description_html() {
        return public_description_html;
    }

    public void setPublic_description_html(String public_description_html) {
        this.public_description_html = public_description_html;
    }

    public boolean isSpoilers_enabled() {
        return spoilers_enabled;
    }

    public void setSpoilers_enabled(boolean spoilers_enabled) {
        this.spoilers_enabled = spoilers_enabled;
    }

    public int[] getIcon_size() {
        return icon_size;
    }

    public void setIcon_size(int[] icon_size) {
        this.icon_size = icon_size;
    }

    public String getSuggested_comment_sort() {
        return suggested_comment_sort;
    }

    public void setSuggested_comment_sort(String suggested_comment_sort) {
        this.suggested_comment_sort = suggested_comment_sort;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public void setIcon_img(String icon_img) {
        this.icon_img = icon_img;
    }

    public String getHeader_title() {
        return header_title;
    }

    public void setHeader_title(String header_title) {
        this.header_title = header_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isUser_is_muted() {
        return user_is_muted;
    }

    public void setUser_is_muted(boolean user_is_muted) {
        this.user_is_muted = user_is_muted;
    }

    public String getSubmit_link_label() {
        return submit_link_label;
    }

    public void setSubmit_link_label(String submit_link_label) {
        this.submit_link_label = submit_link_label;
    }

    public int[] getHeader_size() {
        return header_size;
    }

    public void setHeader_size(int[] header_size) {
        this.header_size = header_size;
    }

    public boolean isPublic_traffic() {
        return public_traffic;
    }

    public void setPublic_traffic(boolean public_traffic) {
        this.public_traffic = public_traffic;
    }

    public String getAccounts_active() {
        return accounts_active;
    }

    public void setAccounts_active(String accounts_active) {
        this.accounts_active = accounts_active;
    }

    public Long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Long subscribers) {
        this.subscribers = subscribers;
    }

    public String getSubmit_text_label() {
        return submit_text_label;
    }

    public void setSubmit_text_label(String submit_text_label) {
        this.submit_text_label = submit_text_label;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public boolean isUser_is_moderator() {
        return user_is_moderator;
    }

    public void setUser_is_moderator(boolean user_is_moderator) {
        this.user_is_moderator = user_is_moderator;
    }

    public String getKey_color() {
        return key_color;
    }

    public void setKey_color(String key_color) {
        this.key_color = key_color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCreated() {
        return created;
    }

    public void setCreated(Double created) {
        this.created = created;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isQuarantine() {
        return quarantine;
    }

    public void setQuarantine(boolean quarantine) {
        this.quarantine = quarantine;
    }

    public boolean isHide_ads() {
        return hide_ads;
    }

    public void setHide_ads(boolean hide_ads) {
        this.hide_ads = hide_ads;
    }

    public Double getCreated_utc() {
        return created_utc;
    }

    public void setCreated_utc(Double created_utc) {
        this.created_utc = created_utc;
    }

    public int[] getBanner_size() {
        return banner_size;
    }

    public void setBanner_size(int[] banner_size) {
        this.banner_size = banner_size;
    }

    public boolean isUser_is_contributor() {
        return user_is_contributor;
    }

    public void setUser_is_contributor(boolean user_is_contributor) {
        this.user_is_contributor = user_is_contributor;
    }

    public String getAdvertiser_category() {
        return advertiser_category;
    }

    public void setAdvertiser_category(String advertiser_category) {
        this.advertiser_category = advertiser_category;
    }

    public String getPublic_description() {
        return public_description;
    }

    public void setPublic_description(String public_description) {
        this.public_description = public_description;
    }

    public boolean isShow_media_preview() {
        return show_media_preview;
    }

    public void setShow_media_preview(boolean show_media_preview) {
        this.show_media_preview = show_media_preview;
    }

    public int getComment_score_hide_mins() {
        return comment_score_hide_mins;
    }

    public void setComment_score_hide_mins(int comment_score_hide_mins) {
        this.comment_score_hide_mins = comment_score_hide_mins;
    }

    public String getSubreddit_type() {
        return subreddit_type;
    }

    public void setSubreddit_type(String subreddit_type) {
        this.subreddit_type = subreddit_type;
    }

    public String getSubmission_type() {
        return submission_type;
    }

    public void setSubmission_type(String submission_type) {
        this.submission_type = submission_type;
    }

    public boolean isUser_is_subscriber() {
        return user_is_subscriber;
    }

    public void setUser_is_subscriber(boolean user_is_subscriber) {
        this.user_is_subscriber = user_is_subscriber;
    }
}
