package com.rappi.carlos_reyes_android.infrastructure.model;

import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class Data {

    private String modhash;
    private List<Children> children;
    private String after;
    private String before;


    @Override
    public String toString() {
        return "Data{" +
                "modhash='" + modhash + '\'' +
                ", children=" + children +
                ", after='" + after + '\'' +
                ", before='" + before + '\'' +
                '}';
    }

    public Data(String modhash, List<Children> children, String after, String before) {
        this.modhash = modhash;
        this.children = children;
        this.after = after;
        this.before = before;
    }

    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    public List<Children> getData() {
        return children;
    }

    public void setData(List<Children> children) {
        this.children = children;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }
}
