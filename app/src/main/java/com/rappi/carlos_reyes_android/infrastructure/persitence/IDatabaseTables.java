package com.rappi.carlos_reyes_android.infrastructure.persitence;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * <p>
 * -
 * Tables and Keys of database
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	3/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public interface IDatabaseTables {

    String TABLE_NOTICES = "PA_TABLE_NOTICES";

    String KEY_ID = "id";
    String KEY_TITLE = "title";
    String KEY_ICON_IMG = "icon_img";
    String KEY_ICON_SIZE0 = "icon_size0";
    String KEY_ICON_SIZE1 = "icon_size1";

    String KEY_HEADER_IMG = "header_img";
    String KEY_HEADER_SIZE0 = "header_size0";
    String KEY_HEADER_SIZE1 = "header_size1";
    String KEY_CATEGORY = "category";
    String KEY_DATE_TIME = "date_time";
    String KEY_USER = "user";
    String KEY_DESCRIPTION_SHORT = "description_short";
    String KEY_DESCRIPTION = "description";
    String KEY_SUSCRIBERS = "suscribers";


}
