package com.rappi.carlos_reyes_android.infrastructure.service;

import com.rappi.carlos_reyes_android.constants.ConstantCommunication;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class ServiceBuilder {

    public static ClientService createService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                //.addInterceptor(interceptor)
                .connectTimeout(ConstantCommunication.TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(ConstantCommunication.TIME_OUT_READ, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(ConstantCommunication.URL_API_REDDIT)
                .client(okHttpClient)
                .build();

        return retrofit.create(ClientService.class);
    }

}
