package com.rappi.carlos_reyes_android.infrastructure.persitence;

import android.database.sqlite.SQLiteDatabase;

import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;

import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	3/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public interface IDatabase {

    void onCreate(SQLiteDatabase db);

    void insertNotices(Notice notice) throws Exception;

    List<Children> getNotices() throws Exception;


}
