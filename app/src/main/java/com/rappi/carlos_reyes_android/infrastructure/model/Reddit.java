package com.rappi.carlos_reyes_android.infrastructure.model;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class Reddit {

    private String kind;
    private Data data;

    public Reddit(String kind, Data data) {
        this.kind = kind;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Reddit{" +
                "kind='" + kind + '\'' +
                ", data=" + data +
                '}';
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
