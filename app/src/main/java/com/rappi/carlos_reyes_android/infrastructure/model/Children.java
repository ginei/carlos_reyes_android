package com.rappi.carlos_reyes_android.infrastructure.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * <p/>
 * - Modelo q contiene la noticia
 * <p>
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class Children implements Serializable {

    private String kind;

    @SerializedName("data")
    private Notice notice;

    @Override
    public String toString() {
        return "Children{" +
                "kind='" + kind + '\'' +
                ", notice=" + notice +
                '}';
    }

    public Children() {
    }

    public Children(String kind, Notice notice) {
        this.kind = kind;
        this.notice = notice;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }
}
