package com.rappi.carlos_reyes_android.ui.activity.detailnotice.presenter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.rappi.carlos_reyes_android.constants.Constants;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;
import com.rappi.carlos_reyes_android.ui.activity.detailnotice.view.INoticeDetailView;
import com.rappi.carlos_reyes_android.utils.DateFormat;

import java.util.Locale;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	2/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public class NoticeDetailPresenterImp implements INoticeDetailPresenter {

    private INoticeDetailView iNoticeDetailView;
    private Context mContext;
    private String TAG = NoticeDetailPresenterImp.class.getSimpleName();

    public NoticeDetailPresenterImp(INoticeDetailView iNoticeDetailView, Context mContext) {
        this.iNoticeDetailView = iNoticeDetailView;
        this.mContext = mContext;

    }


    /**
     * Get data notice
     * @return notice
     */
    @Override
    public Notice getDataNotice() {

        Bundle datos = iNoticeDetailView.getExtras();

        Notice notice = null;
        if (datos != null) {
            try {
                notice = (Notice) datos.getSerializable(Constants.DATA_NOTICE);
                if (notice != null
                        ) {


                    iNoticeDetailView.setNoticeDetailTitle(notice.getTitle());
                    if (notice.getHeader_img() != null)
                        iNoticeDetailView.setNoticeDetailHeader(notice.getHeader_img());
                    iNoticeDetailView.setNoticeDetailCategory(notice.getAdvertiser_category());
                    iNoticeDetailView.setNoticeDetailDatetime(DateFormat.fechaNoticia(Math.round(notice.getCreated())));
                    iNoticeDetailView.setNoticeDetailUser(notice.getDisplay_name());
                    iNoticeDetailView.setNoticeDetailDescription(notice.getDescription());
                    iNoticeDetailView.setNoticeDetailSuscribers(String.format(Locale.getDefault(), "%d", notice.getSubscribers()));

                    if (notice.getHeader_size() != null &&
                            notice.getHeader_size().length > 1) {
                        //iNoticeDetailView.setNoticeDetailSizeHeader(notice.getHeader_size());
                    }
                    return notice;
                }
            } catch (NullPointerException e) {
                Log.e(TAG, "NullPointerException " + e);
            }
        }

        return notice;
    }
}
