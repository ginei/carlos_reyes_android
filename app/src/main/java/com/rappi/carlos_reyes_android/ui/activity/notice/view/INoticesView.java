package com.rappi.carlos_reyes_android.ui.activity.notice.view;

import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;

import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * <p>
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public interface INoticesView {

    void updateAdapterNotices(List<Children> noticeList);

    void viewDetailNotice(Notice notice);

    void swipeRefresh();

    void stopSwipeRefresh();

}
