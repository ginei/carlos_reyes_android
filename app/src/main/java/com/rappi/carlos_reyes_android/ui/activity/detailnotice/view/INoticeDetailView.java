package com.rappi.carlos_reyes_android.ui.activity.detailnotice.view;

import android.os.Bundle;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	2/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public interface INoticeDetailView {

    void setNoticeDetailTitle(String text);

    void setNoticeDetailHeader(String text);

    void setNoticeDetailSizeHeader(int[] value);

    void setNoticeDetailCategory(String text);

    void setNoticeDetailDatetime(String text);

    void setNoticeDetailUser(String text);

    void setNoticeDetailDescription(String text);

    void setNoticeDetailSuscribers(String text);

    Bundle getExtras();

}
