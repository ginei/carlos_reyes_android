package com.rappi.carlos_reyes_android.ui.activity.detailnotice.presenter;

import com.rappi.carlos_reyes_android.infrastructure.model.Notice;

import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	2/02/2017
 * Proyecto: 	carlos_reyes_android
 *
 * ****************************************************************
 */
public interface INoticeDetailPresenter {

    Notice getDataNotice();

}

