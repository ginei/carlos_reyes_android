package com.rappi.carlos_reyes_android.ui.activity.notice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rappi.carlos_reyes_android.R;
import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;
import com.rappi.carlos_reyes_android.ui.activity.notice.NoticesActivity;
import com.rappi.carlos_reyes_android.utils.DateFormat;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	02/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class NoticesAdapter extends RecyclerView.Adapter<NoticesAdapter.NoticesHolder> {

    private Context mContext;
    private List<Children> noticeList;
    private String TAG = NoticesAdapter.class.getSimpleName();

    public NoticesAdapter(Context mContext, List<Children> noticeList) {
        this.noticeList = noticeList;
        this.mContext = mContext;

    }


    @Override
    public NoticesAdapter.NoticesHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notices_item, parent, false);
        ButterKnife.bind(this, v);

        return new NoticesHolder(v);
    }


    @Override
    public void onBindViewHolder(final NoticesHolder viewHolder, final int position) {

        try {

            final Notice notice = noticeList.get(position).getNotice();
            Glide.with(mContext).load(notice.getIcon_img())
                    .error(R.mipmap.ic_launcher).into(viewHolder.notice_item_icono);

            if (notice.getIcon_size() != null &&
                    notice.getIcon_size().length > 1) {
                viewHolder.notice_item_icono.getLayoutParams().height = notice.getIcon_size()[0];
                viewHolder.notice_item_icono.getLayoutParams().width = notice.getIcon_size()[1];
            }
            viewHolder.notice_item_category.setText(notice.getAdvertiser_category());
            viewHolder.notice_item_datetime.setText(DateFormat.fechaNoticia(Math.round(notice.getCreated())));
            viewHolder.notice_item_title.setText(notice.getTitle());
            viewHolder.notice_item_user.setText(notice.getDisplay_name());

            viewHolder.notice_item_description.setText(notice.getPublic_description().equals("") ?
                    notice.getSubmit_text() : notice.getPublic_description()
            );
            viewHolder.notice_item_suscribers.setText(String.format(Locale.getDefault(), "%d", notice.getSubscribers()));

            viewHolder.itemView.
                    setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               ((NoticesActivity) mContext).viewDetailNotice(notice);

                                           }
                                       });


            if (position == noticeList.size() / 2 /* calculate middle element position */) {
                viewHolder.setIsInTheMiddle(true);
            } else {
                viewHolder.setIsInTheMiddle(false);
            }


        } catch (Exception ex) {
            Log.e(TAG, "error->" + ex);
        }

    }


    public static class NoticesHolder extends RecyclerView.ViewHolder {

        private boolean mIsInTheMiddle = false;

        @BindView(R.id.notice_item_icono)
        ImageView notice_item_icono;

        @BindView(R.id.notice_item_category)
        TextView notice_item_category;

        @BindView(R.id.notice_item_datetime)
        TextView notice_item_datetime;

        @BindView(R.id.notice_item_title)
        TextView notice_item_title;

        @BindView(R.id.notice_item_user)
        TextView notice_item_user;

        @BindView(R.id.notice_item_description)
        TextView notice_item_description;

        @BindView(R.id.notice_item_suscribers)
        TextView notice_item_suscribers;

        private NoticesHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

        public boolean getIsInTheMiddle() {
                return mIsInTheMiddle;
        }

        void setIsInTheMiddle(boolean isInTheMiddle) {
            mIsInTheMiddle = isInTheMiddle;
        }
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

}