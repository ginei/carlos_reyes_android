package com.rappi.carlos_reyes_android.ui.activity.notice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.rappi.carlos_reyes_android.R;
import com.rappi.carlos_reyes_android.constants.Constants;
import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Notice;
import com.rappi.carlos_reyes_android.ui.activity.detailnotice.NoticeDetailActivity;
import com.rappi.carlos_reyes_android.ui.activity.notice.adapter.NoticesAdapter;
import com.rappi.carlos_reyes_android.ui.activity.notice.presenter.INoticesPresenter;
import com.rappi.carlos_reyes_android.ui.activity.notice.presenter.NoticesPresenterImp;
import com.rappi.carlos_reyes_android.ui.activity.notice.view.INoticesView;
import com.rappi.carlos_reyes_android.ui.generic.IMenssagesView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Manage the view for the news list
 * <p>
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class NoticesActivity extends AppCompatActivity implements INoticesView, IMenssagesView {

    private Context mContext;
    private String TAG = NoticesActivity.class.getSimpleName();
    private INoticesPresenter iNoticesPresenter;

    private NoticesAdapter noticesAdapter;

    @BindView(R.id.notices_rv)
    RecyclerView mNoticesRv;

    @BindView(R.id.notices_refresh)
    SwipeRefreshLayout mNoticesRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notices);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setTitle(R.string.app_name);
        } catch (Exception ex) {
            Log.e(TAG, "" + ex);
        }


        ButterKnife.bind(this);

        mContext = this;

        iNoticesPresenter = new NoticesPresenterImp(this, mContext);

        iNoticesPresenter.getServiceNotices();

        iNoticesPresenter.getNoticesDatabase();

        swipeRefresh();
    }


    @Override
    public void setLoading() {

    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void messageAlert() {

    }

    /**
     * Update the view the news list
     *
     * @param noticeList list notices
     */
    @Override
    public void updateAdapterNotices(List<Children> noticeList) {

        mNoticesRv.setLayoutManager(new LinearLayoutManager(mContext));
        noticesAdapter = new NoticesAdapter(mContext, noticeList);
        mNoticesRv.setAdapter(noticesAdapter);
        noticesAdapter.notifyDataSetChanged();
    }

    /**
     * Goes to the detail view of the news
     *
     * @param notice News information
     */
    @Override
    public void viewDetailNotice(Notice notice) {
        Bundle datos = new Bundle();
        Intent activityma = new Intent().setClass(
                mContext, NoticeDetailActivity.class);
        datos.putSerializable(Constants.DATA_NOTICE, notice);
        activityma.putExtras(datos);

        mContext.startActivity(activityma);
    }

    /**
     * Refresh notices list
     */
    @Override
    public void swipeRefresh() {
        mNoticesRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                iNoticesPresenter.getServiceNotices();

            }
        });

        mNoticesRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    /**
     * Stop refresh
     */
    @Override
    public void stopSwipeRefresh() {
        mNoticesRefresh.setRefreshing(false);
    }
}
