package com.rappi.carlos_reyes_android.ui.generic;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved

 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public interface IMenssagesView {

    void setLoading();

    void stopLoading();

    void messageAlert();


}
