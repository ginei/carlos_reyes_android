package com.rappi.carlos_reyes_android.ui.activity.notice.presenter;

import android.content.Context;
import android.util.Log;

import com.rappi.carlos_reyes_android.infrastructure.model.Children;
import com.rappi.carlos_reyes_android.infrastructure.model.Reddit;
import com.rappi.carlos_reyes_android.infrastructure.persitence.Database;
import com.rappi.carlos_reyes_android.infrastructure.service.ClientService;
import com.rappi.carlos_reyes_android.infrastructure.service.ServiceBuilder;
import com.rappi.carlos_reyes_android.ui.activity.notice.view.INoticesView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * <p/>
 * - Controller for the news list view
 * <p>
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class NoticesPresenterImp implements INoticesPresenter {

    private INoticesView iNoticesView;
    private Context mContext;
    private String TAG = NoticesPresenterImp.class.getSimpleName();
    private ClientService service;
    private Database mDataBase;

    public NoticesPresenterImp(INoticesView iNoticesView, Context mContext) {
        this.mContext = mContext;
        this.iNoticesView = iNoticesView;
        service = ServiceBuilder.createService();
    }

    /**
     * Consume service notices
     */
    @Override
    public void getServiceNotices() {

        service.getNotices()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<Reddit>>() {

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "Finish");
                        iNoticesView.stopSwipeRefresh();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "error " + e);
                        iNoticesView.stopSwipeRefresh();
                    }

                    @Override
                    public void onNext(Response<Reddit> redditResponse) {

                        Log.d(TAG, "code " + redditResponse.code());
                        int code = redditResponse.code();
                        if (code != 404 && code == 200) {

                            try {

                                List<Children> noticeList = redditResponse.body().getData().getData();

                                setNoticeDatabase(noticeList);

                            } catch (Exception ex) {
                                Log.e(TAG, "error response " + ex.getMessage());
                            }
                        }

                    }
                });
    }

    /**
     * Insert in the database
     *
     * @param noticeList list notices
     */
    @Override
    public void setNoticeDatabase(List<Children> noticeList) {
        mDataBase = Database.getInstance(mContext);

        try {
            for (int i = 0; i < noticeList.size(); i++) {
                mDataBase.insertNotices(noticeList.get(i).getNotice());
            }
            getNoticesDatabase();

        } catch (Exception e) {
            Log.e(TAG, " " + e);
        }

    }

    /**
     * Get the news list
     *
     * @return Notices list
     */
    @Override
    public List<Children> getNoticesDatabase() {
        List<Children> noticeList = new ArrayList<>();
        mDataBase = Database.getInstance(mContext);
        try {
            noticeList = mDataBase.getNotices();
            iNoticesView.updateAdapterNotices(noticeList);
            iNoticesView.stopSwipeRefresh();
        } catch (Exception e) {
            Log.e(TAG, "" + e);
        }
        return noticeList;
    }


}
