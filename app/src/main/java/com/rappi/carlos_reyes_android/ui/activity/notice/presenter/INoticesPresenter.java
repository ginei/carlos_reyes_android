package com.rappi.carlos_reyes_android.ui.activity.notice.presenter;

import com.rappi.carlos_reyes_android.infrastructure.model.Children;

import java.util.List;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	01/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public interface INoticesPresenter {

    void getServiceNotices();

    void setNoticeDatabase(List<Children> noticeList);

    List<Children> getNoticesDatabase();

}
