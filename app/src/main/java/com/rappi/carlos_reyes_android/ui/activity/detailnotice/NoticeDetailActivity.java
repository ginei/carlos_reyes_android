package com.rappi.carlos_reyes_android.ui.activity.detailnotice;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rappi.carlos_reyes_android.R;
import com.rappi.carlos_reyes_android.ui.activity.detailnotice.presenter.INoticeDetailPresenter;
import com.rappi.carlos_reyes_android.ui.activity.detailnotice.presenter.NoticeDetailPresenterImp;
import com.rappi.carlos_reyes_android.ui.activity.detailnotice.view.INoticeDetailView;
import com.rappi.carlos_reyes_android.ui.generic.IMenssagesView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * **************************************************************
 * Copyright (c) 2015 - 2017 Carlos Arturo Reyes Romero, All rights reserved
 * <p>
 * -
 * Manage the view for the news detail
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	2/02/2017
 * Proyecto: 	carlos_reyes_android
 * ****************************************************************
 */
public class NoticeDetailActivity extends AppCompatActivity implements INoticeDetailView, IMenssagesView {

    private Context mContext;

    private String TAG = NoticeDetailActivity.class.getSimpleName();

    private INoticeDetailPresenter iNoticeDetailPresenter;

    @BindView(R.id.notice_detail_header)
    ImageView mNoticeDetailHeader;

    @BindView(R.id.notice_detail_category)
    TextView mNoticeDetailCategory;

    @BindView(R.id.notice_detail_datetime)
    TextView mNoticeDetailDatetime;

    @BindView(R.id.notice_detail_user)
    TextView mNoticeDetailUser;

    @BindView(R.id.notice_detail_description)
    TextView mNoticeDetailDescription;

    @BindView(R.id.notice_detail_suscribers)
    TextView mNoticeDetailSuscribers;

    @BindView(R.id.notice_detail_title)
    TextView mNoticeDetailTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_detail);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setTitle(R.string.app_name);
        } catch (NullPointerException ex) {
            Log.e(TAG, "" + ex);
        }

        ButterKnife.bind(this);
        mContext = this;

        iNoticeDetailPresenter = new NoticeDetailPresenterImp(this, mContext);

        iNoticeDetailPresenter.getDataNotice();

    }


    @Override
    public void setLoading() {
    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void messageAlert() {
    }

    /**
     * Set title notice
     * @param text text notice
     */
    @Override
    public void setNoticeDetailTitle(String text) {
        mNoticeDetailTitle.setText(text);
    }

    /**
     * Set image notice
     * @param text
     */
    @Override
    public void setNoticeDetailHeader(String text) {
        Glide.with(mContext).load(text).error(R.mipmap.ic_launcher).into(mNoticeDetailHeader);
    }

    /**
     * set size header image notice
     * @param value
     */
    @Override
    public void setNoticeDetailSizeHeader(int[] value) {
        mNoticeDetailHeader.getLayoutParams().height = value[0];
        mNoticeDetailHeader.getLayoutParams().width = value[1];
    }

    /**
     * set category notice
     * @param text category
     */
    @Override
    public void setNoticeDetailCategory(String text) {
        mNoticeDetailCategory.setText(text);
    }

    /**
     * set datetime created notice
     * @param text datetime
     */
    @Override
    public void setNoticeDetailDatetime(String text) {
        mNoticeDetailDatetime.setText(text);
    }

    /**
     * set user
     * @param text user
     */
    @Override
    public void setNoticeDetailUser(String text) {
        mNoticeDetailUser.setText(text);
    }

    /**
     * set description notice
     * @param text description
     */
    @Override
    public void setNoticeDetailDescription(String text) {
        mNoticeDetailDescription.setText(text);
    }

    /**
     * set suscribers notice
     * @param text number suscribers
     */
    @Override
    public void setNoticeDetailSuscribers(String text) {
        mNoticeDetailSuscribers.setText(text);
    }

    /**
     * getting extras activity
     * @return extras
     */
    @Override
    public Bundle getExtras() {
        return getIntent().getExtras();
    }
}
