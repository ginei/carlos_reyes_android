package com.rappi.carlos_reyes_android.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * **************************************************************
 * Copyright (c) 2017 - 2017, All rights reserved
 *
 * - Format the dates
 * -
 * Autor:		Carlos Arturo Reyes Romero
 * email:		carr900@gmail.com
 * Creado:   	02/02/2017
 * Proyecto: 	carlos-reyes-android
 * ****************************************************************
 */
public class DateFormat {

    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);

    /**
     * Return date format dd/MM/yy HH:mm
     *
     * @param time timestamp
     * @return dd/MM/yy HH:mm
     */
    public static String fechaNoticia(Long time) {
        Date date = new Date(time*1000);
        return sdf.format(date);
    }

}
