package com.rappi.carlos_reyes_android;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.rappi.carlos_reyes_android.ui.activity.notice.NoticesActivity;
import com.rappi.carlos_reyes_android.ui.activity.notice.adapter.NoticesAdapter;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class NoticesListTest {

    private static final int ITEM = 5;

    @Rule
    public ActivityTestRule<NoticesActivity> mActivityRule = new ActivityTestRule<>(
            NoticesActivity.class);


    /**
     * Scroll the item and click
     */
    @Test
    public void scrollToItem() {

        onView(ViewMatchers.withId(R.id.notices_rv))
                .perform(RecyclerViewActions.actionOnItemAtPosition(ITEM, click()));

    }


    /**
     * Scroll halfway and check if you have text "app_name"
     */
    @Test
    public void itemHasText() {

        onView(ViewMatchers.withId(R.id.notices_rv))
                .perform(RecyclerViewActions.scrollToHolder(isInTheMiddle()));

        String middleElementText =
                mActivityRule.getActivity().getResources().getString(R.string.app_name);
        onView(withText(middleElementText)).check(matches(isDisplayed()));
    }


    private static Matcher<NoticesAdapter.NoticesHolder> isInTheMiddle() {
        return new TypeSafeMatcher<NoticesAdapter.NoticesHolder>() {
            @Override
            protected boolean matchesSafely(NoticesAdapter.NoticesHolder customHolder) {
                return customHolder.getIsInTheMiddle();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("item in the middle");
            }
        };
    }

}
